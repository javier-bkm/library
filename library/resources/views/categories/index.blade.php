@extends('layouts.app')

@section('content')


    <div class="flex justify-center flex-wrap bg-gray-200 p-4 mt-5">

        <div class="text-center">
        
            <h1 class="mb-5">
                {{ __("List of categories") }}
            </h1>

            <a href="{{ route('categories.create') }}" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
            
                {{ __('Create a new category') }}

            </a>

        </div>

    </div>

    <table class="border-separate border-2 text-center border-gray-500 mt-3" style="width: 100%;">

        <thead>
            <tr>
            
                <th class="px-4 py2">{{ __('Name') }}</th>
                <th class="px-4 py2">{{ __('Description') }}</th>
            
            </tr>
        
        </thead>

        <tbody>
        
            @forelse( $category as $cat )

                <tr>
                    <td class="border px-4 py-2">{{ $cat -> name }}</td>
                    <td class="border px-4 py-2">{{ $cat -> description }}</td>
                    <td class="border px-4 py-2">
                        <a href="{{ route('categories.edit', ['category' => $cat]) }}" class="text-blue-400"> {{ __('Edit') }} </a>

                        <a 
                            href="#"
                            class="text-red-400"
                            onclick="event.preventDefault();
                                    document.getElementById('delete-category-{{ $cat -> id }}-form').submit();"
                        >{{ __('Delete') }}
                        </a>

                        <form id="delete-category-{{ $cat -> id }}-form" action="{{ route('categories.destroy', ['category' => $cat]) }}" method="POST" class="hidden">

                            @method("DELETE")
                            @csrf

                        </form>

                    </td>
                    
                </tr>

            @empty

                <tr>

                    <td colspan="5">
                        
                        <div class="bg-red-100 text-center border border-red-400 text-red-700 px-4 py-3">
                        
                            <p>
                                <strong class="font-bold">
                                    {{ __("There are no categories") }}
                                </strong>
                                <span class="block sm:inline">
                                    {{ __("registered yet") }}
                                </span>
                            </p>
                        
                        </div>

                    </td>
                
                </tr>

            @endforelse
        
        </tbody>
    
    </table>

    @if( $category -> count() )
        
        <div class="mt-3">
        
            {{ $category -> links() }}
        
        </div>

    @endif

@endsection