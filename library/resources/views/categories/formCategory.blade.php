
<div class="w-full max-w-lg">

    <div class="flex flex-wrap">
    
        <h1 class="mb-5"> {{ __("$title") }} </h1>

        <form class="w-full max-w-lg" method="POST" action="{{ $route }}">

        @csrf
        @isset($update)
            @method("PUT")
        @endisset

        <!-- Name of Category -->
        <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="name">
                    {{ __("Name") }}
                </label>
                <input name="name" value=" {{ old('name') ?? $category -> name }}" class="no-resize appareance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 resize-none" type="text">
                <!-- <p class="text-gray-600 text-xs italic mt-3"> {{ __("Name of the book") }} </p> -->
                @error("name")

                    <div class="border border-red-400 rounded-b bg-red-100 mt-1 px-4 py-3 text-red-700">
                        {{ $message }}
                    </div>

                @enderror
            </div>
        </div>


        <!-- Description of Category -->
        <div class="flex flex-wrap -mx-3 mb-6">

            <div class="w-full px-3">
            
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="description">
                    {{ __("Description") }}
                </label>

                <textarea name="description" id="description" class="no-resize appareance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 h-48 resize-none" cols="30" rows="10">{{ old("description") ?? $category -> description }}</textarea>

                @error("description")

                    <div class="border border-red-400 rounded-b bg-red-100 mt-1 px-4 py-3 text-red-700">
                        {{ $message }}
                    </div>

                @enderror
            
            </div>

        </div>

        <div class="md:flex md:items-center">

            <div class="md:2-1/3">
            
                <button class="shadow bg-teal-400 hover:bg-teal-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit">
                    {{ $textButton }}
                </button>

            </div>

        </div>

        </form>


    </div>

</div>
