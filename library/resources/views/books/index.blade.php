@extends('layouts.app')

@section('content')


    <div class="flex justify-center flex-wrap bg-gray-200 p-4 mt-5">

        <div class="text-center">
        
            <h1 class="mb-5">
                {{ __("List of books") }}
            </h1>

            <a href="{{ route('books.create') }}" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
            
                {{ __('Create a new book') }}

            </a>

        </div>

    </div>

    <table class="border-separate border-2 text-center border-gray-500 mt-3" style="width: 100%;">

        <thead>
            <tr>
            
                <th class="px-4 py2">{{ __('Name') }}</th>
                <th class="px-4 py2">{{ __('Author') }}</th>
                <th class="px-4 py2">{{ __('Category') }}</th>
                <th class="px-4 py2">{{ __('Publication date') }}</th>
                <th class="px-4 py2">{{ __('Available') }}</th>
                <th class="px-4 py2">{{ __('Actions') }}</th>
            
            </tr>
        
        </thead>

        <tbody>
        
            @forelse( $books as $book )

                <tr>
                    <td class="border px-4 py-2">{{ $book -> name }}</td>
                    <td class="border px-4 py-2">{{ $book -> user -> name }}</td>
                    <td class="border px-4 py-2">{{ $book -> category }}</td>
                    <td class="border px-4 py-2">{{ $book -> publication_date }}</td>
                    <td class="border px-4 py-2">

                        @if ( isset($book -> user_borrowing) )
                            {{ __("Borrowed by" ) }} {{ $book -> user_borrowing }}
                        @else
                            {{ __("Available") }}
                        @endif 

                    </td>
                    <td class="border px-4 py-2">

                        <a href="{{ route('books.edit', ['book' => $book]) }}" class="text-blue-400"> {{ __('Edit') }} </a>

                        <a 
                            href="#"
                            class="text-red-400"
                            onclick="event.preventDefault();
                                    document.getElementById('delete-book-{{ $book -> id }}-form').submit();"
                        >{{ __('Delete') }}
                        </a>

                        <form id="delete-book-{{ $book -> id }}-form" action="{{ route('books.destroy', ['book' => $book]) }}" method="POST" class="hidden">

                            @method("DELETE")
                            @csrf

                        </form>

                    </td>
                    
                </tr>

            @empty

                <tr>

                    <td colspan="6">
                        
                        <div class="bg-red-100 text-center border border-red-400 text-red-700 px-4 py-3">
                        
                            <p>
                                <strong class="font-bold">
                                    {{ __("There are no books") }}
                                </strong>
                                <span class="block sm:inline">
                                    {{ __("registered yet") }}
                                </span>
                            </p>
                        
                        </div>

                    </td>
                
                </tr>

            @endforelse
        
        </tbody>
    
    </table>

    @if( $books -> count() )
        
        <div class="mt-3">
        
            {{ $books -> links() }}
        
        </div>

    @endif


@endsection