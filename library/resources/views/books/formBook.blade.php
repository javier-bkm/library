
<div class="w-full max-w-lg">

    <div class="flex flex-wrap">
    
        <h1 class="mb-5"> {{ __("$title") }} </h1>

        <form class="w-full max-w-lg" method="POST" action="{{ $route }}">
        
        @csrf
        @isset($update)
            @method("PUT")
        @endisset

        <!-- Name of book -->
        <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="name">
                    {{ __("Name") }}
                </label>
                <input name="name" value="{{ old('name') ?? $book -> name }}" class="no-resize appareance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 resize-none" type="text">
                @error("name")

                    <div class="border border-red-400 rounded-b bg-red-100 mt-1 px-4 py-3 text-red-700">
                        {{ $message }}
                    </div>

                @enderror
            </div>
        </div>

        <!-- Author of book -->
        <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="author">
                    {{ __("Author") }}
                </label>
                <input name="author" value="{{ old('author') ?? $book -> author }}" class="no-resize appareance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 resize-none" type="text">
                @error("author")

                    <div class="border border-red-400 rounded-b bg-red-100 mt-1 px-4 py-3 text-red-700">
                        {{ $message }}
                    </div>

                @enderror
            </div>
        </div>


        <!-- Category of book -->
        <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="category">
                    {{ __("Category") }}
                </label>
            
                <select id="category" name="category" class="no-resize appareance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 resize-none">
                        <option value="">Ninguna</option>
                        @forelse( $category as $cat )
                            <option value="{{ $cat -> name }}" {{ $book -> category == $cat -> name ? 'selected' : ''  }} name="{{ $cat -> name }}" id="{{ $cat -> id }}" >{{ $cat -> name }}</option>
                        @empty
                        @endforelse
                </select>

            </div>
        </div>

        <!-- Borrow a book -->
        <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="user_borrowing">
                    {{ __("Borrow a book") }}
                </label>
            
                <select id="user_borrowing" name="user_borrowing" class="no-resize appareance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 resize-none">
                        <option value="">Available</option>
                        @forelse( $users as $user )
                            <option value="{{ $user -> name }}" {{ $book -> user_borrowing == $user -> name ? 'selected' : ''  }} name="{{ $user -> name }}" id="{{ $user -> id }}" >{{$user -> name }}</option>
                        @empty
                        @endforelse
                </select>

            </div>
        </div>

        <!-- Author of book -->
        <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="publication_date">
                    {{ __("Publication date") }}
                </label>
                <input name="publication_date" value=" {{ old('publication_date') ?? $book -> publication_date }}" class="no-resize appareance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 resize-none" type="text">
                @error("author")

                    <div class="border border-red-400 rounded-b bg-red-100 mt-1 px-4 py-3 text-red-700">
                        {{ $message }}
                    </div>

                @enderror
            </div>
        </div>

        <div class="md:flex md:items-center">

            <div class="md:2-1/3">
            
                <button class="shadow bg-teal-400 hover:bg-teal-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit">
                    {{ $textButton }}
                </button>

            </div>

        </div>

        </form>


    </div>

</div>

