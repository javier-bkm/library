<?php

namespace Database\Factories;

use App\Models\Books;
use Illuminate\Database\Eloquent\Factories\Factory;

class BooksFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Books::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this -> faker -> company,
            'author' => $this -> faker -> paragraph(3),
            'user_id' => User::all() -> random() -> id,
            'user_borrowing' => $this -> faker -> company,
            'created_at' => now()
        ];
    }
}
