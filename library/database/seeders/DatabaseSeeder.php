<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\User::factory()->times(1) -> create([

            'name' => 'javier.martinez',
            'email' => 'javier.bkm@gmail.com',
            'password' => bcrypt('password')
    
         ]);
    
         \App\Models\Project::factory()->times(40)->create();

    }




}
