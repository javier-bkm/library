<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'author', 'user_borrowing', 'publication_date', 'category'];

    protected static function boot(){
        
        parent::boot();

        self::creating(function ($table){
            if( ! app()-> runningInConsole() ){

                $table -> user_id = auth() -> id();

            }
        });

    }

    public function user(){

        return $this -> belongsTo(User::class);

    }


}
