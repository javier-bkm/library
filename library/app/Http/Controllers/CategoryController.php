<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function __construct(){
        $this -> middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::with('user')->paginate(10);
        return view('categories.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category;
        $title = __("Create a new category");
        $textButton = __("Create");
        $route = route("categories.store");
        return view( "categories.createCategory", compact("title", "textButton", "route", "category") );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this -> validate($request, [
            "name" => "required|unique:category",
            "description" => "nullable|string"

        ]);

        Category::create($request -> only("name", "description"));
        return redirect( route("categories.index") )
            ->with("success", __("Category has been created!"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $update = true;
        $title = __("Update Category");
        $textButton = __("Update");
        $route = route("categories.update", ["category" => $category]);
        return view("categories.editCategory", compact("update", "title", "textButton", "route", "category"));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this -> validate($request, [

            "name" => "required|unique:category,name," . $category -> id,
            "description" => "nullable|string"

        ]);

        $category -> fill( $request -> only("name", "description")) -> save();
        return back() -> with("success", "Category has been updated!");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category -> delete();

        return back() -> with("success", ("Category has been deleted!"));

    }
}
