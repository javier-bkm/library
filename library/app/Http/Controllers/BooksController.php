<?php

namespace App\Http\Controllers;

use App\Models\Books;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;

class BooksController extends Controller
{

    public function __construct(){
        $this -> middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Books::with('user')->paginate(10);
        return view('books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $book = new Books;
        $category = Category::all();
        $users = User::all();
        $title = __("Create a new book");
        $textButton = __("Create");
        $route = route("books.store");
        return view( "books.createBook", compact("title", "textButton", "route", "book", "category", "users") );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this -> validate($request, [

            "name" => "required|unique:books",
            "author" => "nullable|string",
            "publication_date" => "nullable|string",
            "user_borrowing" => "nullable",
            "category" => "nullable|string"
        ]);
        
        //dd($request);

        Books::create($request -> only("name", "author", "publication_date", "category", "user_borrowing"));
        return redirect( route("books.index") )
            ->with("success", __("Book has been created!"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Books  $books
     * @return \Illuminate\Http\Response
     */
    public function show(Books $books)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Books  $books
     * @return \Illuminate\Http\Response
     */
    public function edit(Books $book)
    {
        $update = true;
        $title = __("Update book");
        $category = Category::all();
        $users = User::all();
        $textButton = __("Update");
        $route = route("books.update", ['book' => $book]);
        return view("books.editBook", compact("update", "title", "textButton", "route", "book", "category", "users"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Books  $books
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Books $book)
    {
        $this -> validate($request, [

            "name" => "required|unique:books,name," . $book -> id,
            "author" => "nullable|string",
            "category" => "nullable|string",
            "publication_date" => "nullable|string",
            "user_borrowing" => "nullable|string"

        ]);

        $book -> fill( $request -> only("name", "author", "category", "publication_date", "user_borrowing")) -> save();
        return back() -> with("success", "¡Book has been updated!");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Books  $books
     * @return \Illuminate\Http\Response
     */
    public function destroy(Books $book)
    {
        $book -> delete();

        return back() -> with("success", ("Book has been deleted!"));
    }
}
