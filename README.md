# library

1) Instalar dependencias y configuraciones iniciales de laravel dentro de la carpeta library

- npm install
- composer install
- php artisan key:generate

2) Configurar base de datos y ambiente

- Crear una base de datos
- Copiar y pegar el archivo .env.example y renombrarlo como .env
- Configurar la conexión a BD en el archivo .env
- Ingresar el key generado (obtenerlo de .env) en el archivo app.php

4) Crear tablas de base de datos necesarias

- php artisan migrate

3) Iniciar el servidor

- php artisan serve